package com.jaguarlabs.baseproject.components;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.jaguarlabs.baseproject.R;
import com.jaguarlabs.baseproject.fragments.bases.BaseFragment;
import com.jaguarlabs.baseproject.utilities.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * Wrappers the basic fragment transactions like add, hide and remove into easy to use methods which control
 * the management of fragments.
 *
 * Some classes which already uses a child helper along its interfaces implementation are:<br>
 *
 *     <u>
 *         <li>BaseActivity</li>
 *         <li>BaseFragment</li>
 *     </u>
 *
 *  <br> Version 1.2 - Carlos Briseno modified on 15/06/2016
 *  <br> Version 1.1 - Carlos Briseno modified on 05/05/2016
 *  <br> Version 1.0 - Carlos Briseno carlosbrisoft@gmail.com
 *
 */
public class ChildHelper {

	//region variables

	//region constants

	private static final String TAG_CONSTANT                        = "is_constant";

	public static final int     FRAGMENT_CONTAINER_NO_SPECIFIED     = 0;
	public static final int     ACTION_BAR_TITLE_NO_SPECIFIED       = 0;
	public static final int     MENU_NO_SPECIFIED                   = 0;
	public static final int     MENU_IGNORE                         = 1;
	public static final int     TITLE_IGNORE                        = 0;
	public static final int     HIDE_ACTIONBAR                      = -1;

	//endregion

	//region objects

	public ArrayList<String>    mConstantTagsList                   = null;
	public ArrayList<String>    mTagsList                           = null;

	private Contract            mContract                           = null;
    private Animation           mAnimation                          = Animation.NoAnimation;
	private Animation 			mTempAnimation 						= null;
    private HashMap<String, Animation> hashMap                      = new HashMap<>();

	//endregion

	//region primitives

	private String              HOME_FRAGMENT_TAG                   = null;
	private boolean             mIsNextFragmentConstant             = false;
	private boolean             mHasThisBeenDestroyed               = false;
	public int                  mFragmentCounter                    = 0;
    private boolean             mFragmentContainerAdded             = false;
	private static int 			ID_COUNTER 							= 231;
    private int                 mFragmentContainerId                = ID_COUNTER;

	//endregion

	//endregion

	//region Life cycle

	/**
	 *
	 * @param contract The implementation which will be used during all the life cycle of this object
	 */
	public ChildHelper(@NonNull Contract contract){ID_COUNTER++;
		this.mContract = contract;
		mTagsList = new ArrayList<>();
		mConstantTagsList = new ArrayList<>();
	}

	public Bundle onSaveInstanceState(){
		Bundle bundle = new Bundle();

		bundle.putSerializable("mTagsList", mTagsList);
		bundle.putSerializable("mConstantTagsList", mConstantTagsList);
		bundle.putInt("mFragmentCounter", mFragmentCounter);
		bundle.putString("HOME_FRAGMENT_TAG", HOME_FRAGMENT_TAG);

		return bundle;
	}

	public void onRestoreInstanceState(Bundle bundle){
		mTagsList = (ArrayList<String>) bundle.getSerializable("mTagsList");
		mConstantTagsList = (ArrayList<String>) bundle.getSerializable("mConstantTagsList");
		mFragmentCounter = bundle.getInt("mFragmentCounter", 0);
		HOME_FRAGMENT_TAG = bundle.getString("HOME_FRAGMENT_TAG", null);

		FragmentTransaction transaction = mContract.getContractFragmentManager().beginTransaction();

		//Hides previous fragment

		if (mFragmentCounter > 0){

			for(String tag: mTagsList){
				transaction.hide(mContract.getContractFragmentManager().findFragmentByTag(tag));
			}
			for(String tag: mConstantTagsList){
				transaction.hide(mContract.getContractFragmentManager().findFragmentByTag(tag));
			}

			String tag = mTagsList.get(mTagsList.size()-1);
			BaseFragment fragment = (BaseFragment) mContract.getContractFragmentManager().findFragmentByTag(tag);

			transaction.show(fragment);
			transaction.commit();

			setUpActionBar(fragment);
		}

		mIsNextFragmentConstant = false;
	}

	/**
	 * It should be called when your activity or fragment is being destroyed, otherwhise, it could try to do transactions
	 * after its activity/fragment host has died
	 */
	public void onDestroy(){
		mHasThisBeenDestroyed = true;
	}

	//endregion

	//region methods

	//region setters

	public void setHomeFragmentTag(String tag){
		HOME_FRAGMENT_TAG = tag;
	}

    public void setAnimation(Animation animation){
        mAnimation = animation;
    }

    public void setTemporalAnimation(Animation animation){
        mTempAnimation = animation;
    }

	//endregion

	//region actionbar related

	public boolean onCreateOptionsMenu(Menu menu){
		boolean optionsHandled = false;

		if (getCount() > 0){
			BaseFragment baseFragment = getCurrent();

			if (baseFragment != null){

				//If the current fragment has its own childHelper, pass to it the event of the menu so the baseFragment's children can have a chance to handle the event
				if (baseFragment.getChildHelper() != null){
					optionsHandled = baseFragment.getChildHelper().onCreateOptionsMenu(menu);
				}

				//If the event was not handled and the the current fragment handles its fragment by its own instead of using a childHelper,
				//that's the case for BasePagerFragment, then gets the BasePagerFragment's current child in order to pass to it the events of the menu
				if (!optionsHandled && baseFragment instanceof CustomHelper){
					CustomHelper customHelper = (CustomHelper) baseFragment;

					if (customHelper.getChildContract() != null){
						ChildHelper childHelper = customHelper.getChildContract().getChildHelper();

						if (childHelper != null){
							optionsHandled = customHelper.getChildContract().getChildHelper().onCreateOptionsMenu(menu);
						}
					}
				}

				//If the baseFragment's children did not handle the event, give to the baseFragment a chance to handle it by its own
				if (!optionsHandled){
					optionsHandled = handleMenuOptions(baseFragment.getActionBarTitleResId(), baseFragment.getActionBarMenuResId(), menu);
				}
			}

			//If the baseFragment did not handle the event, give to its parent a chance to handle it.
			if (!optionsHandled){
				optionsHandled = handleMenuOptions(mContract.getActionBarTitleResId(), mContract.getActionBarMenuResId(), menu);
			}
		} else if (mContract instanceof CustomHelper && ((CustomHelper) mContract).getChildContract() != null && ((CustomHelper) mContract).getChildContract().getChildHelper() != null) {
            optionsHandled = ((CustomHelper) mContract).getChildContract().getChildHelper().onCreateOptionsMenu(menu);
        }
        else{
            optionsHandled = handleMenuOptions(mContract.getActionBarTitleResId(), mContract.getActionBarMenuResId(), menu);
        }

		return optionsHandled;
	}

	public boolean onOptionsItemSelected(MenuItem menuItem){
		boolean optionsHandled = false;

		if (getCount() > 0){
			BaseFragment baseFragment = getCurrent();

			if (baseFragment != null){

				//If the current fragment has its own childHelper, pass to it the event of the menu so the baseFragment's children can have a chance to handle the event
				if (baseFragment.getChildHelper() != null){
					optionsHandled = baseFragment.getChildHelper().onOptionsItemSelected(menuItem);
				}

				//If the event was not handled and the the current fragment handles its fragment by its own instead of using a childHelper,
				//that's the case for BasePagerFragment, then gets the BasePagerFragment's current child in order to pass to it the events of the menu
				if (!optionsHandled && baseFragment instanceof CustomHelper){
					CustomHelper customHelper = (CustomHelper) baseFragment;

					if (customHelper.getChildContract() != null){
						ChildHelper childHelper = customHelper.getChildContract().getChildHelper();

						if (childHelper != null){
							optionsHandled = customHelper.getChildContract().getChildHelper().onOptionsItemSelected(menuItem);
						}
					}
				}

				//If the baseFragment's children did not handle the event, give to the baseFragment a chance to handle it by its own
				if (!optionsHandled){
					optionsHandled = baseFragment.onOptionsItemSelected(menuItem);
				}
			}

			//If the baseFragment did not handle the event, give to its parent a chance to handle it.
			if (!optionsHandled && mContract instanceof BaseFragment){
				optionsHandled = ((BaseFragment)mContract).onOptionsItemSelected(menuItem);
			}

            //If the baseFragment did not handle the event, give to its parent a chance to handle it.
            if (!optionsHandled && mContract instanceof BaseFragment){
                optionsHandled = ((BaseFragment)mContract).onOptionsItemSelected(menuItem);
            }
		}else if (getCount() == 0){
			if (mContract instanceof BaseFragment){
				optionsHandled = ((BaseFragment)mContract).onOptionsItemSelected(menuItem);
			}
		}

		return optionsHandled;

//		if (getCount() > 0){
//			BaseFragment baseFragment = getCurrent();
//
//			if (baseFragment.getActionbarTitleStringId() != ACTIONBAR_TITLE_HIDE &&
//					baseFragment.getActionbarTitleStringId() != TITLE_IGNORE &&
//					baseFragment.getActionbarMenuId() != ACTION_BAR_MENU_OMIT &&
//					Util.UI.isResourceId(mContract.getContractActivity(), baseFragment.getActionbarMenuId())){
//
//				result = baseFragment.onOptionsItemSelected(menuItem);
//			}
//		}
	}

	private boolean handleMenuOptions(final int titleResId, final int menuResId, Menu menu){
		boolean menuOptionsHandled = false;

		if (titleResId != HIDE_ACTIONBAR && menuResId != HIDE_ACTIONBAR){

			if (menuResId == MENU_NO_SPECIFIED){
				//This means a fragment does not currently need options menu
				menuOptionsHandled = true;
				menu.clear();
			}
			else if (menuResId == MENU_IGNORE){
				//This means a fragment should keep the previos options menu
				menuOptionsHandled = false;
			}
			else if (isResourceId(menuResId)){
				//This means a fragment with supply its own options menu

				MenuInflater menuInflater = new MenuInflater(mContract.getContractActivity());
				menuInflater.inflate(menuResId, menu);
				menuOptionsHandled = true;

                if (mContract instanceof Fragment) {
                    ((Fragment) mContract).onCreateOptionsMenu(menu, menuInflater);
                }

				if (!mContract.getContractActionBar().isShowing()){
					mContract.getContractActionBar().show();
				}
			}
		}
		else if(mContract.getContractActionBar().isShowing()){
			mContract.getContractActionBar().hide();
			menuOptionsHandled = true;
		}

		return menuOptionsHandled;
	}

	private boolean handleActionBar(final int titleResId, final int menuResId){
		boolean actionBarHandled = false;

		if (titleResId != HIDE_ACTIONBAR && menuResId != HIDE_ACTIONBAR){

			if (titleResId != TITLE_IGNORE){
				if (titleResId != ACTION_BAR_TITLE_NO_SPECIFIED && isResourceId(titleResId)){

					mContract.getContractActionBar().setTitle(titleResId);

					actionBarHandled = true;

					//TODO:Verify if the next if and the call to invalidateOptions menu does not create a weird actionbar animation
					Util.LOG.w("Verify weird actionbar animation(?)");
					if (!mContract.getContractActionBar().isShowing()){
						mContract.getContractActionBar().show();
					}

					//Previous code, it was not changing the actionbar menu when a fragment has MENU_IGNORE
//					if (menuResId != MENU_IGNORE){
//						if (menuResId != MENU_NO_SPECIFIED && isResourceId(menuResId)){
//							mContract.getContractActivity().invalidateOptionsMenu();
//						}
//					}
					mContract.getContractActivity().invalidateOptionsMenu();
				}
			}
			else{
				Util.LOG.i("WEIRD CASE IN TITLE");
				actionBarHandled = true;
			}
		}
		else if(mContract.getContractActionBar().isShowing()){
			mContract.getContractActionBar().hide();
			actionBarHandled = true;
		}

		return actionBarHandled;
	}

	public void setUpActionBar(){
		BaseFragment baseFragment = getCurrent();

		setUpActionBar(baseFragment);
	}

	private boolean setUpActionBar(BaseFragment fragment){
		boolean titleHandle = false;

		//Verify if the fragment has children, if so, pass them the event to give them a chance to handle the event
		if (fragment != null && fragment.getChildHelper() != null){
			titleHandle = fragment.getChildHelper().setUpActionBar(fragment.getChildHelper().getCurrent());
		}

		//if the fragment is not null, use its data to set up the actionbar
		if (!titleHandle && fragment != null){
			titleHandle = handleActionBar(fragment.getActionBarTitleResId(), fragment.getActionBarMenuResId());
		}

		//If previous validations did not handle the event, use host fragment to set up the action bar
		if (!titleHandle){
			titleHandle = handleActionBar(mContract.getActionBarTitleResId(), mContract.getActionBarMenuResId());
		}

		return titleHandle;
//        int titleResId = fragment.getActionbarTitleStringId();
//        int menuResId = fragment.getActionbarMenuId();
//
//        if (titleResId == ACTIONBAR_TITLE_HIDE){
//            if (mContract.getContractActionBar().isShowing()){
//                mContract.getContractActionBar().hide();
//            }
//        }
//        else if (titleResId != TITLE_IGNORE){
//
//            if (Util.UI.isResourceId(mContract.getContractActivity(), titleResId)){
//                mContract.getContractActionBar().setTitle(mContract.getContractActivity().getString(titleResId));
//            }
//
//            if (menuResId != ACTION_BAR_MENU_OMIT && Util.UI.isResourceId(mContract.getContractActivity(), menuResId)){
//                mContract.getContractActivity().invalidateOptionsMenu();
//            }
//            if (!mContract.getContractActionBar().isShowing()){
//                mContract.getContractActionBar().show();
//            }
//        }
	}

	//endregion

	//region transactions wrapper

	public String addChildFragmentConstant(BaseFragment baseFragment){
		return  addChildFragmentConstant(baseFragment, false);
	}

	public String addChildFragmentConstant(BaseFragment fragment, boolean clearStack){
		mIsNextFragmentConstant = true;
		return addChildFragment(fragment, clearStack);
	}

	public String addChildFragment(BaseFragment baseFragment){
		return addChildFragment(baseFragment, false);
	}

    @SuppressWarnings("ResourceType")
	public String addChildFragment(BaseFragment fragment, boolean clearStack){
		//Generates a unique tag for the incoming fragment
		String tag = mIsNextFragmentConstant ? fragment.getName()+"_"+TAG_CONSTANT+"_"+mConstantTagsList.size() : fragment.getName() +"_"+ mFragmentCounter;
		fragment.setParentContract(mContract);
		//TODO: Consider the next case if needed, To remove the fragment if some how there was a fragment with the same recently generated tag (?) weird right?
//		if (mContract.getContractFragmentManager().findFragmentByTag(tag) != null){
//			mContract.getContractFragmentManager().beginTransaction().remove(mContract.getContractFragmentManager().findFragmentByTag(tag)).commitAllowingStateLoss();
//		}

		//Begins needed transactions
		FragmentTransaction transaction = mContract.getContractFragmentManager().beginTransaction();
        if (mTempAnimation != null){
            hashMap.put(tag, mTempAnimation);
            transaction.setCustomAnimations(mTempAnimation.getResourceIdEnter(), mTempAnimation.getResourceIdExit(), mTempAnimation.getResourceIdEnter(), mTempAnimation.getResourceIdExit());
            mTempAnimation = null;
        } else if (mAnimation != Animation.NoAnimation) {
            transaction.setCustomAnimations(mAnimation.getResourceIdEnter(), mAnimation.getResourceIdExit(), mAnimation.getResourceIdEnter(), mAnimation.getResourceIdExit());
        }

		//Hides previous fragment
		if (mTagsList.size() > 0){
			String previousFragmentTag = mTagsList.get(mTagsList.size()-1);
			Fragment previousFragment = mContract.getContractFragmentManager().findFragmentByTag(previousFragmentTag);
			transaction.hide(previousFragment);
		}

		if(clearStack){
			String homeTag = null;

			if (HOME_FRAGMENT_TAG != null){
				for(String possibleHomeTag: mConstantTagsList){
					if (possibleHomeTag.toLowerCase().contains(HOME_FRAGMENT_TAG.toLowerCase())){
						homeTag = possibleHomeTag;
						break;
					}
				}
			}

			for(String tagToRemove: mTagsList){
				if (!tagToRemove.contains(TAG_CONSTANT)){
					Fragment previousFragment = mContract.getContractFragmentManager().findFragmentByTag(tagToRemove);
					transaction.remove(previousFragment);
				}
				else if (!tagToRemove.equals(tag)){
					Fragment previousFragment = mContract.getContractFragmentManager().findFragmentByTag(tagToRemove);
					transaction.hide(previousFragment);
				}
			}

			if (homeTag != null && !homeTag.equalsIgnoreCase(tag)){
				mFragmentCounter = 1;
				mTagsList.clear();
				mTagsList.add(homeTag);
			}else{
				mFragmentCounter = 0;
				mTagsList.clear();
			}
		}
        if (mContract.getFragmentContainerResId() == FRAGMENT_CONTAINER_NO_SPECIFIED && !mFragmentContainerAdded) {
            mFragmentContainerAdded = true;
            View viewRoot = mContract.getRootView();

            if (viewRoot == null){
                throw new RuntimeException("Cannot add fragment due to FragmentContainerResId was 0 and rootView was null");
            }else if (!(viewRoot instanceof ViewGroup)){
                throw new RuntimeException("Cannot add fragment due to FragmentContainerResId was 0 and rootView is not an instance of ViewGroup");
            }else{
                ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                FrameLayout frameLayout = new FrameLayout(viewRoot.getContext());
                frameLayout.setLayoutParams(layoutParams);
                frameLayout.setId(mFragmentContainerId);
                ((ViewGroup) viewRoot).addView(frameLayout);
            }
        }

        if (mContract.getFragmentContainerResId() != FRAGMENT_CONTAINER_NO_SPECIFIED){
            transaction.add(mContract.getFragmentContainerResId(), fragment, tag);
        }else{
            transaction.add(mFragmentContainerId, fragment, tag);
        }

		mFragmentCounter++;
		mTagsList.add(tag);
		transaction.commit();
		mContract.getContractFragmentManager().executePendingTransactions();

		setUpActionBar(fragment);

		if (mIsNextFragmentConstant && !mConstantTagsList.contains(tag)){
			mConstantTagsList.add(tag);
		}

		mIsNextFragmentConstant = false;

		return tag;
	}

	/**
	 *
	 * @return true if the was a child fragment to remove, otherwise false.
	 */
	public boolean removeLastChild(){
		if (!mHasThisBeenDestroyed && mTagsList.size() > 0){
			return removeSpecificChild(mTagsList.get(mTagsList.size()-1), false);
		}

		return false;
	}

	public boolean removeSpecificChild(String tag, boolean removeIfConstant){
		if (mTagsList.size() == 0){
			return false;
		}
		else{

			for(String fragmentTag: mTagsList){
				if (fragmentTag.equals(tag)){
					mFragmentCounter--;
					FragmentTransaction transaction = mContract.getContractFragmentManager().beginTransaction();
                    if (hashMap.containsKey(tag)){
                        Animation animation = hashMap.get(tag);
//                        hashMap.remove(tag);
                        transaction.setCustomAnimations(animation.getResourceIdInverseEnter(), animation.getResourceIdInverseExit(), animation.getResourceIdInverseEnter(), animation.getResourceIdInverseExit());
                    }
                    else if (mAnimation != Animation.NoAnimation) {
                        transaction.setCustomAnimations(mAnimation.getResourceIdInverseEnter(), mAnimation.getResourceIdInverseExit(), mAnimation.getResourceIdInverseEnter(), mAnimation.getResourceIdInverseExit());
                    }

					if (!removeIfConstant && fragmentTag.contains(TAG_CONSTANT)){
						transaction.hide(mContract.getContractFragmentManager().findFragmentByTag(fragmentTag));
					}
					else{
						if (removeIfConstant)
							mConstantTagsList.remove(fragmentTag);
						transaction.remove(mContract.getContractFragmentManager().findFragmentByTag(fragmentTag));
					}

					transaction.commitAllowingStateLoss();

					mTagsList.remove(fragmentTag);

					if (mTagsList.size() > 0){
						String previousFragmentTag = mTagsList.get(mFragmentCounter -1);
						BaseFragment previousFragment = (BaseFragment) mContract.getContractFragmentManager().findFragmentByTag(previousFragmentTag);

						setUpActionBar(previousFragment);
                        if (hashMap.containsKey(tag)){
                            Animation animation = hashMap.get(tag);
                            hashMap.remove(tag);
                            mContract.getContractFragmentManager().beginTransaction().setCustomAnimations(animation.getResourceIdInverseEnter(), animation.getResourceIdInverseExit(), animation.getResourceIdInverseEnter(), animation.getResourceIdInverseExit()).show(previousFragment).commitAllowingStateLoss();
                        }
                        else if (mAnimation != Animation.NoAnimation) {
                            mContract.getContractFragmentManager().beginTransaction().setCustomAnimations(mAnimation.getResourceIdInverseEnter(), mAnimation.getResourceIdInverseExit(), mAnimation.getResourceIdInverseEnter(), mAnimation.getResourceIdInverseExit()).show(previousFragment).commitAllowingStateLoss();
                        }else{
                            mContract.getContractFragmentManager().beginTransaction().show(previousFragment).commitAllowingStateLoss();
                        }
					}
					else{
						//TO_DO Use parent actionbar and validate thi: DONE
						//TO_DO get parent title: DONE
						//TODO verify if the actionbar is being isRecreated twice, first with
						//the following line and then with the method setUpActionBar, that's due both methods
						//call invalidateOptionsMenu();
						mContract.getContractActivity().invalidateOptionsMenu();
						setUpActionBar(null);
					}

					return true;
				}
			}

			return  false;
		}
	}

	public void removeAllChildren(){
		List<Fragment> fragmentList = new ArrayList<>();

		for(String tag: mTagsList){
			Fragment fragment = mContract.getContractFragmentManager().findFragmentByTag(tag);

			if (fragment != null){
				fragmentList.add(fragment);
			}
		}

		for(String tag: mConstantTagsList){
			Fragment fragment = mContract.getContractFragmentManager().findFragmentByTag(tag);

			if (fragment != null && !fragmentList.contains(fragment)){
				fragmentList.add(fragment);
			}
		}

		if (fragmentList.size() > 0){
			FragmentTransaction transaction = mContract.getContractFragmentManager().beginTransaction();
			mTagsList.clear();
			mConstantTagsList.clear();
			mFragmentCounter = 0;

			for(Fragment fragment: fragmentList){
				transaction.remove(fragment);
			}

			transaction.commitAllowingStateLoss();
			mContract.getContractActivity().invalidateOptionsMenu();
		}
	}

	public void showConstantChild(String tagToSearch, boolean clearStack){

		FragmentTransaction transaction = mContract.getContractFragmentManager().beginTransaction();

		String homeTag = null;

        if (mTempAnimation != null){
            hashMap.put(tagToSearch, mTempAnimation);
            transaction.setCustomAnimations(mTempAnimation.getResourceIdEnter(), mTempAnimation.getResourceIdExit(), mTempAnimation.getResourceIdEnter(), mTempAnimation.getResourceIdExit());
            mTempAnimation = null;
        } else if (mAnimation != Animation.NoAnimation) {
            transaction.setCustomAnimations(mAnimation.getResourceIdEnter(), mAnimation.getResourceIdExit(), mAnimation.getResourceIdEnter(), mAnimation.getResourceIdExit());
        }

		if (HOME_FRAGMENT_TAG != null){
			for(String possibleHomeTag: mConstantTagsList){
				if (possibleHomeTag.toLowerCase().contains(HOME_FRAGMENT_TAG.toLowerCase())){
					homeTag = possibleHomeTag;
					break;
				}
			}
		}

		for(String tag: mConstantTagsList){
			if (tag.equals(tagToSearch)){

				if(clearStack){
					for(String tagToRemove: mTagsList){
						if (!tagToRemove.contains(TAG_CONSTANT)){
							Fragment fragment = mContract.getContractFragmentManager().findFragmentByTag(tagToRemove);
							transaction.remove(fragment);
						}
						else if (!tagToRemove.equals(tag)){
							Fragment fragment = mContract.getContractFragmentManager().findFragmentByTag(tagToRemove);
							transaction.hide(fragment);
						}
					}

					if (homeTag != null && !homeTag.equalsIgnoreCase(tag)){
						mFragmentCounter = 1;
						mTagsList.clear();
						mTagsList.add(homeTag);
					}else{
						mFragmentCounter = 0;
						mTagsList.clear();
					}
				}

				BaseFragment fragment = (BaseFragment)mContract.getContractFragmentManager().findFragmentByTag(tag);
				fragment.onBringToFront();
				transaction.show(fragment).commit();
				mTagsList.add(tag);
				mFragmentCounter++;

				setUpActionBar(fragment);
				break;
			}
		}

	}

	//endregion

	//region utilities

	private boolean isResourceId(int resId){
		return Util.UI.isResourceId(mContract.getContractActivity(), resId);
	}

	public BaseFragment getCurrent(){
		if (mTagsList.size() == 0){
			return null;
		}

		String tag = mTagsList.get(mFragmentCounter -1);
		BaseFragment currentFragment = (BaseFragment) mContract.getContractFragmentManager().findFragmentByTag(tag);

		return currentFragment;
	}

	public boolean isInList(String tag){

		if (tag != null){
			for(String tagInList: mConstantTagsList){
				if (tagInList.equals(tag)){
					return true;
				}
			}

			for(String tagInList: mTagsList){
				if (tagInList.equals(tag)){
					return true;
				}
			}
		}

		return false;
	}

	public BaseFragment getFragment(String tag){

		for(String tagInList: mConstantTagsList){
			if (tagInList.equals(tag)){
				return (BaseFragment) mContract.getContractFragmentManager().findFragmentByTag(tagInList);
			}
		}

		for(String tagInList: mTagsList){
			if (tagInList.equals(tag)){
				return (BaseFragment) mContract.getContractFragmentManager().findFragmentByTag(tagInList);
			}
		}

		return null;
	}

	public int getCount(){
		return mTagsList.size();
	}

	//endregion

	//endregion

	//region inner definitions

	public interface Contract{
		int getActionBarTitleResId();
		int getActionBarMenuResId();
		int getFragmentContainerResId();
		View getRootView();
		AppCompatActivity getContractActivity();
		FragmentManager getContractFragmentManager();
		ActionBar getContractActionBar();
		ChildHelper getChildHelper();
	}

	public interface CustomHelper{
		Contract getChildContract();
	}

	public enum Animation{
		NoAnimation(0,0,0,0),
		FromBottomToTop(R.anim.enter_from_bottom, R.anim.exit_to_top, R.anim.enter_from_top, R.anim.exit_to_bottom),
		FromLeftToRight(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right),
		FadeInFadeOut(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);

        private final  int resourceIdEnter, resourceIdExit, resourceIdInverseEnter, resourceIdInverseExit;

        Animation(final int resourceIdEnter, final int resourceIdExit, final int resourceIdInverseEnter, final int resourceIdInverseExit){
            this.resourceIdEnter = resourceIdEnter;
            this.resourceIdExit = resourceIdExit;
            this.resourceIdInverseEnter = resourceIdInverseEnter;
            this.resourceIdInverseExit = resourceIdInverseExit;
        }

        public int getResourceIdEnter() {
            return resourceIdEnter;
        }

        public int getResourceIdExit() {
            return resourceIdExit;
        }

        public int getResourceIdInverseEnter() {
            return resourceIdInverseEnter;
        }

        public int getResourceIdInverseExit() {
            return resourceIdInverseExit;
        }
    }

	//endregion
}
