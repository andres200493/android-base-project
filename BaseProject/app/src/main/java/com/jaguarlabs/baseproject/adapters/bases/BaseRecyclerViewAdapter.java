package com.jaguarlabs.baseproject.adapters.bases;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.jaguarlabs.baseproject.views.bases.BaseViewHolder;

import java.util.ArrayList;

/**
 * Created by Arturo on 09/05/2016.
 */
public abstract class BaseRecyclerViewAdapter<T, V extends View> extends RecyclerView.Adapter<BaseViewHolder<V>> {
    protected ArrayList<T> items =  new ArrayList<T>();

    public ArrayList<T> getItems (){
        return items;
    }

    public void setItems(ArrayList<T> items){
        this.items = items;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public BaseViewHolder<V> onCreateViewHolder(ViewGroup parent, int viewType) {
        return new BaseViewHolder<V>(onCreateItemView(parent,viewType));
    }

    protected abstract V onCreateItemView(ViewGroup parent, int viewType);
}
