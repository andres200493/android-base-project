package com.jaguarlabs.baseproject.services;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jaguarlabs.baseproject.config.Const;

import org.greenrobot.eventbus.EventBus;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Carlos Briseño on 09/05/2016.
 */
public class BackendService extends IntentService{

    //region common

    private static final String EXTRA_REQUEST_CODE = "com.jaguarlabs.baseproject.services.EXTRA_REQUEST_CODE";
    private static final long DEFAULT_REQUEST_CODE = -1;
    private long mRequestCode;

    public BackendService() {
        super("BackendService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mRequestCode = intent.getLongExtra(EXTRA_REQUEST_CODE, DEFAULT_REQUEST_CODE);
    }

    private static Intent getBaseIntent(Context context, String action){
        Intent intent = new Intent(context, BackendService.class);
        intent.setAction(action);
        return intent;
    }

    private static void init(Context context, Intent intent){
        context.startService(intent);
    }

    private Retrofit getRetrofit(boolean excludeFieldsWithoutExposeAnnotation){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        return new Retrofit.Builder()
                .baseUrl(Const.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(excludeFieldsWithoutExposeAnnotation?new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create():new Gson()))
                .client(client)
                .build();
    }

    private void sendEvent(Object object) {
        EventBus.getDefault().post(object);
    }

    //endregion

    //region A service

    //region get

    //endregion

    //region post
    //endregion

    //region put
    //endregion

    //region delete
    //endregion

    //endregion
}
