package com.jaguarlabs.baseproject.activities;

import android.support.v7.widget.Toolbar;

import com.jaguarlabs.baseproject.R;
import com.jaguarlabs.baseproject.activities.bases.BaseActivity;

import org.androidannotations.annotations.EActivity;

@EActivity(R.layout.activity_main)
public class MainActivity extends BaseActivity{

    //region Variables X

    //region Constants X
    //endregion

    //region Primitives X
    //endregion

    //region Objects X
    //endregion

    //region Views X
    //endregion

    //endregion

    //region Component life cycle

    @Override
    protected void main(boolean isRecreated) {
        super.main(isRecreated);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (!isRecreated){
            //TODO: Do something
        }
    }

    //endregion

    //region Methods

    //region UI clicks X
    //endregion

    //region Actionbar clicks X
    //endregion

    //region Private X
    //endregion

    //region Protected X
    //endregion

    //region Public X

    //region Getters X
    //endregion

    //region Setters X
    //endregion

    //endregion

    //region Super override

    @Override
    public int getActionBarTitleResId() {
        return 0;
    }

    @Override
    public int getActionBarMenuResId() {
        return 0;
    }

    @Override
    public int getFragmentContainerResId() {
        return R.id.fragment_container;
    }

    //endregion

    //region Interface implementations X

    //region example-onClickListener
    //endregion

    //region example-onLocationListener
    //endregion

    //region example-onCustomActionListener
    //endregion

    //endregion

    //endregion

    //region Inner definitions X

    //region Interfaces X

    //endregion

    //region Classes X
    //endregion

    //endregion

}