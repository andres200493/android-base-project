package com.jaguarlabs.baseproject.fragments.bases;

import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;

import com.jaguarlabs.baseproject.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Carlos Briseño on 25/02/2016.
 */
@EFragment
public abstract class BaseDrawerFragment extends BaseFragment{

    DrawerLayout drawer;

    //region life cycle

    @AfterViews
    public void mainDrawer(){
        drawer = getDrawer();
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                getActivity(), drawer, getToolbar(), R.string.navigation_drawer_close, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public boolean onBackPress() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            closeDrawer();
            return true;
        } else {
            return super.onBackPress();
        }
    }

    //endregion

    //region methods

    protected void closeDrawer(){
        drawer.closeDrawer(GravityCompat.START);
    }

    //endregion

    //region abstract methods

    public abstract DrawerLayout getDrawer();

    //endregion
}
