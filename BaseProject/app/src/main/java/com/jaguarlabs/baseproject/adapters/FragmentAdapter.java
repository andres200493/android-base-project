package com.jaguarlabs.baseproject.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;

import com.jaguarlabs.baseproject.fragments.bases.BaseFragment;
import com.jaguarlabs.baseproject.fragments.bases.BaseSinglePageFragment;

import java.lang.ref.SoftReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Carlos Briseño on 23/02/2016.
 */
public class FragmentAdapter extends FragmentPagerAdapter{

    private List<BaseSinglePageFragment> fragments;
    private Context mContext;

    public FragmentAdapter(Context context, FragmentManager fm, List<BaseSinglePageFragment> basePageFragmentList) {
        super(fm);
        mContext = context;
        this.fragments = basePageFragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return  fragments.get(position);
    }

    public BaseFragment getFragmentAtPosition(int position){
        return fragments.get(position);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mContext.getString(fragments.get(position).getPageTitleResId());
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

}
