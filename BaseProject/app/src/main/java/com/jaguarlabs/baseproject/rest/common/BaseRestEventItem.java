package com.jaguarlabs.baseproject.rest.common;

/**
 * Created by Carlos Briseño on 09/05/2016.
 */
public abstract class BaseRestEventItem<T> {

    private RestStatus restStatus;
    private T item;
    private long requestCode;

    public BaseRestEventItem(RestStatus status, T item) {
        this.restStatus = status;
        this.item = item;
    }

    public RestStatus getRestStatus() {
        return restStatus;
    }

    public T getItem() {
        return item;
    }

    public long getRequestCode() {
        return requestCode;
    }

    public void setRequestCode(long requestCode) {
        this.requestCode = requestCode;
    }
}
