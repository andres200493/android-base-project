package com.jaguarlabs.baseproject.utilities;

import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringU {

	public static String removeFirstSpace(CharSequence s){
		int chars = s.length();
		
		if (chars > 0){
			if (s.charAt(0) == 32){
				if (chars == 1){
					return "";
				}
				else{
					return s.toString().substring(1, chars);
				}
			}
		}
		
		return s.toString();
	}
	
	public static void removeContinuousSpaces(EditText etxt){
		int length = etxt.length();
		String text = etxt.getText().toString();
		if (length > 2){
			for(int i = 0; i < length; i++){
				if (i >= 1 && i != (length-1) && text.charAt(i) == 32 && text.charAt(i+1) == 32){
					etxt.setText(text.replace("  ", " "));
					etxt.setSelection(i+1);
					break;
				}
			}
		}
	}
	
	public static void removeSpecialCharacters(EditText etxt){//Allow unicode characters like
		String text = etxt.getText().toString();
		
		Pattern p = Pattern.compile("[^\\p{L}\\p{Z}0-9]");
	     Matcher m = p.matcher(text);
	     
	     if (m.find()){ //If true there is a special character
	        etxt.setText(text.replaceAll("[^\\p{L}\\p{Z}0-9]", ""));
	        etxt.setSelection(etxt.length());
	     }
	}
	
	public static void keepOnlyLetters(EditText etxt){//Allow unicode characters like
		String text = etxt.getText().toString();
		
		Pattern p = Pattern.compile("[^\\p{L}\\p{Z}]");
	     Matcher m = p.matcher(text);
	     
	     if (m.find()){ //If true there is a special character
	        etxt.setText(text.replaceAll("[^\\p{L}\\p{Z}]", ""));
	        etxt.setSelection(etxt.length());
	     }
	}
	
	public static void keepOnlyNumbers(EditText etxt){
		String text = etxt.getText().toString();
		
		Pattern p = Pattern.compile("[^0-9]");
	     Matcher m = p.matcher(text);
	     
	     if (m.find()){
	        etxt.setText(text.replaceAll("[^0-9]", ""));
	        etxt.setSelection(etxt.length());
	     }
	}

	public static void keepOnlyDecimalNumber(EditText etxt, int integerLimit, int decimalLimit){
		String text = etxt.getText().toString();
		
		Pattern p = Pattern.compile("[^0-9.]");
	    Matcher m = p.matcher(text);
	    boolean isInvalidDecimalString = false;
	    boolean hasDot = false;
	    boolean hasSurpassedDecimalLimit = false;
	    boolean hasSurpassedIntegerLimit = false;
	    boolean respectSelectedPosition = true;
	    
	    if (etxt.length() > 0){
            int selection = etxt.getSelectionStart();

            if (m.find()){
                text = (text.replaceAll("[^0-9.]", ""));
                if (text.length() > 0 && text.charAt(0) == '.'){
                    text = "0"+text;
                }

                isInvalidDecimalString = true;
            }
            else{
                if (text.length() > 0 && text.charAt(0) == '.'){
                    text = "0"+text;
                    isInvalidDecimalString = true;
                    respectSelectedPosition = false;
                }
            }

            if (integerLimit != WatcherUtil.NO_INTEGER_LIMIT){
                hasDot = text.contains(".");

                if (!hasDot && text.length() > integerLimit){

                    if (text.charAt(integerLimit) != '.'){
                        StringBuilder builder = new StringBuilder(text);
                        builder.deleteCharAt(etxt.getSelectionStart()-1);
                        text = builder.toString();
                        hasSurpassedIntegerLimit = true;
                    }
                }
                else if(hasDot){

                    int integerCounter = 0;

                    for(int i = 0; text.charAt(i) != '.'; i++){
                        integerCounter++;
                    }

                    if (integerCounter > integerLimit){
                        StringBuilder builder = new StringBuilder(text);
                        builder.deleteCharAt(etxt.getSelectionStart()-1);
                        text = builder.toString();
                        hasSurpassedIntegerLimit = true;
                    }
                }
            }

            int dotIndex = -1;

            for(int i = 0; i < text.length(); i++){
                if (text.charAt(i) == '.'){
                    int decimalCounter = 0;
                    dotIndex = i;

                    for(int j = dotIndex+1; j < text.length(); j++){
                        decimalCounter++;
                    }

                    if (decimalCounter > decimalLimit){
                        StringBuilder builder = new StringBuilder(text);
                        builder.deleteCharAt(etxt.getSelectionStart()-1);
                        text = builder.toString();
                        hasSurpassedDecimalLimit = true;
                    }
                    break;
                }
            }

            if (hasSurpassedDecimalLimit || isInvalidDecimalString || hasSurpassedIntegerLimit){
                etxt.setText(text);

                if (respectSelectedPosition && (etxt.length() >= (selection-1))){
                    etxt.setSelection(selection-1);
                }
                else{
                    etxt.setSelection(etxt.length());
                }
            }
        }
	}
	
	public static boolean hasSpaces(String text){
		int length = text.length();
		
		for(int i = 0; i < length; i++){
			if (text.charAt(i) == 32){
				return true;
			}
		}
		
		return false;
	}
	
	public static String removeAllSpaces(String text){
		int length = text.length();
		
		for(int i = 0; i < length; i++){
			if (text.charAt(i) == 32){
				return text.replace(" ", "");
			}
		}
		
		return text;
	}
	
	 public static boolean hasOnlyNumbers(String text){
			
		Pattern p = Pattern.compile("[^0-9]");
	     Matcher m = p.matcher(text);
	     
	     if (m.find()){
	        return false;
	     }
	     
	     return true;
	}
	 
	 public static String keepOnlyNumbers(String text){
			
		Pattern p = Pattern.compile("[^0-9]");
	     Matcher m = p.matcher(text);
	     
	     if (m.find()){
	    	 return text.replaceAll("[^0-9]", "");
	     }
	     
	     return text;
	}
}
