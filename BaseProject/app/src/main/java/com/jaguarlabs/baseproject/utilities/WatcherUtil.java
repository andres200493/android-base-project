package com.jaguarlabs.baseproject.utilities;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

public class WatcherUtil implements TextWatcher{

	public static final int TYPE_NONE = 0, TYPE_ALPHABETICAL = 2;
	boolean allowSpaces = true;
	boolean allowContiuosSpaces = true;
	boolean allowInitialSpace = true;
	boolean allowSpecialCharacters = true;
	boolean allowOnlyNumbers = false;
	boolean allowOnlyDecimalNumers = false;
	int charLimit = 0;
	public static int NO_INTEGER_LIMIT = -1;
	int integerLimit = NO_INTEGER_LIMIT;
	int decimalLimit = 10;
	private int type = TYPE_NONE;
	
	EditText etxt;
	
	public WatcherUtil(EditText etxt){
		this.etxt = etxt;
	}
	
	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		
		if (allowOnlyNumbers){
			StringU.keepOnlyNumbers(etxt);
		}
		else if (allowOnlyDecimalNumers){
			StringU.keepOnlyDecimalNumber(etxt, integerLimit,decimalLimit);
		}
		else{
			if (!allowSpaces){
				String text = s.toString();
				if (StringU.hasSpaces(text)){
					etxt.setText(StringU.removeAllSpaces(text));
					etxt.setSelection(etxt.length());
				}
			}
			else{
				if (!allowInitialSpace){
					if (count > 0 && s.charAt(0) == 32){
						etxt.setText(StringU.removeFirstSpace(s));
						return;
					}
				}
				
				if (!allowContiuosSpaces){
					StringU.removeContinuousSpaces(etxt);
				}
			}
			
			if (type != TYPE_NONE){
				switch(type){
				case TYPE_ALPHABETICAL:
					StringU.keepOnlyLetters(etxt);
					break;
				}
			}
			else if (!allowSpecialCharacters){
				StringU.removeSpecialCharacters(etxt);
			}
		}
		
	}
	
	@Override public void beforeTextChanged(CharSequence s, int start, int count, int after) { 
		
	}
	@Override public void afterTextChanged(Editable s) {
		if (charLimit > 0 && s.length() > charLimit){
			etxt.setText(s.toString().substring(0, charLimit));
			etxt.setSelection(etxt.length());
		}
	}

	
	public WatcherUtil disableSpaces(){
		allowSpaces = false;
		return this;
	}
	
	public WatcherUtil disableContinuousSpaces(){
		allowContiuosSpaces = false;
		return this;
	}
	
	public WatcherUtil disableInitialSpace(){
		allowInitialSpace = false;
		return this;
	}
	
	public WatcherUtil disableSpecialCharacters(){
		allowSpecialCharacters = false;
		return this;
	}
	
	public WatcherUtil setForNumbersOnly(){
		allowOnlyNumbers = true;
		return this;
	}
	
	public WatcherUtil setForDecimalNumbersOnly(){
		allowOnlyDecimalNumers = true;
		return this;
	}
	
	public WatcherUtil setMaxCharacters(int value){
		charLimit = value;
		return this;
	}
	
	public WatcherUtil setMaxDecimalNumbers(int value){
		decimalLimit = value;
		return this;
	}
	
	public WatcherUtil setMaxIntegerNumbers(int value){
		if (value <= 0){
			integerLimit = NO_INTEGER_LIMIT;
		}
		else{
			integerLimit = value;
		}
		
		return this;
	}
	
	public WatcherUtil setType(int type){
		this.type = type;
		return this;
	}
}
