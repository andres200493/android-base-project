package com.jaguarlabs.baseproject.fragments.bases;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;

import com.jaguarlabs.baseproject.R;
import com.jaguarlabs.baseproject.components.ChildHelper;
import com.jaguarlabs.baseproject.utilities.Util;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by Carlos Briseño on 22/03/2016.
 *
 * Version 1.1 Modified on 05/05/2016 by Carlos Briseño
 */
@EFragment
public abstract class BaseFragment extends Fragment implements ChildHelper.Contract {

    private View                    vLoader                         = null;

    private ChildHelper             childHelper                     = null;
    private ChildHelper.Contract    parentContract                  = null;

    /**
     * Used by fragments in order to notice if the fragment is isRecreated
     */
    @InstanceState
    public boolean                  isRecreated                     ;//Do not initialize this variable, it will be initialized when a onRestoreInstanceState happens

    private String                  mBaseTag                        = "BaseFragment";
    protected boolean               mLog                            = true;
    private boolean                 manageEventRegister             = false;

    //region life cycle

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        li("OnCreate" + getName());

        childHelper = new ChildHelper(this);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null){
            childHelper.onRestoreInstanceState(savedInstanceState.getBundle("child"));
        }
    }

    @AfterViews
    public void baseMain(){
        main(isRecreated);

        if (!isRecreated) {
            isRecreated = true;
        }
    }

    protected void main(boolean isRecreated){

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (childHelper != null && childHelper.getCurrent() != null){
            childHelper.getCurrent().onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (getCurrentFragment() != null){
            getCurrentFragment().onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBundle("child", childHelper.onSaveInstanceState());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        le("OnDestroy " + getName());
        childHelper.onDestroy();

        if (manageEventRegister && EventBus.getDefault().isRegistered(this)){
            unregisterForEvents();
        }
    }

    /**
     * A method used to know if a fragments consumed the back press event of an Activity
     * @return {@code true} if the fragment consumed the event, otherwise it returns {@code false}
     */
    public boolean onBackPress() {
        boolean result = false;

        if (childHelper != null){
            if (childHelper.getCount() > 0){
                result = childHelper.getCurrent().onBackPress();

                if (!result){
                    result = onLastChanceToHandleBackPress();

                    if (!result) {
                        childHelper.removeLastChild();
                        result = true;
                    }
                }
            }
        }

        return result;
    }

    public void onBringToFront(){}

    protected void removeSelf(){
        if (parentContract != null){
            parentContract.getChildHelper().removeSpecificChild(getTag(), false);
        }
    }

    protected boolean onLastChanceToHandleBackPress(){
        return false;
    }

    //endregion

    //region methods

    //region log

    protected void setLogTag(String tag){
        mBaseTag = tag;
    }

    protected void setLogEnable(boolean value){
        mLog = value;
    }

    /**
     * Wrapper of Log.i(TAG, MESSAGE)
     * @param message The message you want to be displayed the logcat
     */
    protected void li(String message) {
        if (mLog) {
            Util.LOG.i(mBaseTag, message);
        }
    }

    /**
     * Wrapper of Log.d(TAG, MESSAGE)
     * @param message The message you want to be displayed the logcat
     */
    protected void ld(String message) {
        if (mLog) {
            Util.LOG.d(mBaseTag, message);
        }
    }

    /**
     * Wrapper of Log.w(TAG, MESSAGE)
     * @param message The message you want to be displayed the logcat
     */
    protected void lw(String message) {
        if (mLog) {
            Util.LOG.w(mBaseTag, message);
        }
    }

    /**
     * Wrapper of Log.e(TAG, MESSAGE)
     * @param message The message you want to be displayed the logcat
     */
    protected void le(String message) {
        if (mLog) {
            Util.LOG.e(mBaseTag, message);
        }
    }

    //endregion

    //region user messages

    /**
     * Displays an animated notification to the user, it wont be dismissed unless you call {@link #dismissCurrentMessage()}
     * @param resId The string resource id of the string you want to display
     */
    public void showErrorIndefinite(int resId){
        Util.UI.showErrorIndefinite(getActivity(), resId,(ViewGroup)getActivity().findViewById(R.id.notificationContainer));
    }

    /**
     * Displays an animated error notification to the user, it will be dismissed after 3 seconds you called the method.
     * @param resId The string resource id of the string you want to display
     */
    public void showError(int resId){
        Util.UI.showError(getActivity(), resId, (ViewGroup) getActivity().findViewById(R.id.notificationContainer));
    }

    /**
     * Displays an animated success notification to the user, it will be dismissed after 3 seconds you called the method.
     * @param resId The string resource id of the string you want to display
     */
    public void showSuccess(int resId){
        Util.UI.showSuccess(getActivity(), resId, (ViewGroup) getActivity().findViewById(R.id.notificationContainer));
    }

    /**
     * Displays an animated alert notification to the user, it will be dismissed after 3 seconds you called the method.
     * @param resId The string resource id of the string you want to display
     */
    public void showAlert(int resId){
        Util.UI.showAlert(getActivity(), resId, (ViewGroup) getActivity().findViewById(R.id.notificationContainer));
    }

    /**
     * If there is a message displayed to the user, this methods dismisses it. Methods which can show messages are
     *
     * <br>{@link #showSuccess(int)}
     * <br>{@link #showError(int)}
     * <br>{@link #showAlert(int)}
     * <br>{@link #showErrorIndefinite(int)}
     */
    public void dismissCurrentMessage(){
        Util.UI.dismissCurrentMessage();
    }

    //endregion

    //region utilities

    protected View findViewById(int resId){
        if (getView() != null){
            return getView().findViewById(resId);
        }else{
            return null;
        }
    }

    protected void toast(String message){
        Util.UI.showToast(getActivity(), message);
    }

    protected void toast(@StringRes int stringResId){
        Util.UI.showToast(getActivity(), stringResId);
    }

    public String getName(){
        return this.getClass().getName();
    }

    public void setParentContract(ChildHelper.Contract parentContract){
        this.parentContract = parentContract;
    }

    protected Toolbar getToolbar(){
        return (Toolbar) getActivity().findViewById(R.id.toolbar);
    }

    protected Context getApplicationContext(){
        return getActivity().getApplicationContext();
    }

    protected boolean hasConnection(){
        return Util.Device.hasConnection(getApplicationContext());
    }

    protected void addLoader(){
        if (vLoader == null && getView() != null && getView() instanceof ViewGroup){
            vLoader = View.inflate(getActivity(),R.layout.loader, null);
            vLoader.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            ((ViewGroup)getView()).addView(vLoader);
            vLoader.bringToFront();
        }
    }

    protected void removeLoader(){
        if (vLoader != null && getView() != null && getView() instanceof ViewGroup){
            ((ViewGroup)getView()).removeView(vLoader);
            vLoader = null;
        }
    }

    //region event bus wrapper

    protected void setManageEventRegister(boolean manageEventRegister){
        this.manageEventRegister = manageEventRegister;
        if (manageEventRegister && !EventBus.getDefault().isRegistered(this)){
            registerForEvents();
        }
    }

    /**
     * Sends an object event at application level
     * @param event The object event to be sent
     */
    protected void sendEvent(Object event){
        EventBus.getDefault().post(event);
    }

    protected void registerForEvents(){
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }

    protected void unregisterForEvents(){
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }

    //endregion

    //region child helper wrapper

    public String addFragment(BaseFragment baseFragment){
        return getChildHelper().addChildFragment(baseFragment);
    }

    public String addConstantFragment(BaseFragment baseFragment){
        return getChildHelper().addChildFragmentConstant(baseFragment);
    }

    public String addConstantFragment(BaseFragment baseFragment, boolean clearStack){
        return getChildHelper().addChildFragmentConstant(baseFragment, clearStack);
    }

    public BaseFragment getSpecificFragment(String tag){
        return getChildHelper().getFragment(tag);
    }

    public BaseFragment getCurrentFragment(){
        return getChildHelper().getCurrent();
    }

    public boolean removeSpecificFragment(String tag){
        return getChildHelper().removeSpecificChild(tag, false);
    }

    public boolean removeLastFragment(){
        return getChildHelper().removeLastChild();
    }

    public void removeAllFragments(){
        getChildHelper().removeAllChildren();
    }

    public void showConstantFragment(String tag, boolean clearStack){
        getChildHelper().showConstantChild(tag, clearStack);
    }

    public boolean isFragmentInList(String tag){
        return getChildHelper().isInList(tag);
    }

    public int getFragmentCount(){
        return getChildHelper().getCount();
    }

    //endregion

    //endregion

    //endregion

    //region interface implementations

    //region ChildHelper.Contract

    @Override
    public AppCompatActivity getContractActivity() {
        return ((AppCompatActivity)getActivity());
    }

    @Override
    public FragmentManager getContractFragmentManager() {
        return getChildFragmentManager();
    }

    @Override
    public ActionBar getContractActionBar() {
        return ((AppCompatActivity)getActivity()).getSupportActionBar();
    }

    @Override
    public ChildHelper getChildHelper() {
        return childHelper;
    }

    @Override
    public int getFragmentContainerResId() {
        return 0;
    }

    @Override
    public View getRootView() {
        return getView();
    }

    //endregion

    //endregion
}