package com.jaguarlabs.baseproject.views.bases;

/**
 * Created by Arturo on 09/05/2016.
 */

import android.support.v7.widget.RecyclerView;
import android.view.View;

public class BaseViewHolder<V extends View> extends RecyclerView.ViewHolder {
    public View view;

    public BaseViewHolder(View itemView) {
        super(itemView);
        view = itemView;
    }

    public V getView(){
        return (V) view;
    }
}
